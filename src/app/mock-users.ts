import {User} from './user';

export const USERS: User[] = [
	{id: 1, name:'Ali baba', email:'ali@baba.com'},
	{id: 2, name:'Mauro dos Santos', email:'Mauro@baba.com'},
	{id: 3, name:'Mike Plato', email:'Miko@baba.com'}
];